var renderer, scene, camera, clock, delta, stats,
	uniforms, shaderMaterial, terrain, geometry, vertCount,
	changedVerts, start, vertArr;

// Setup function
function setup() {
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
	clock = new THREE.Clock();
	delta = clock.getDelta();
	renderer = new THREE.WebGLRenderer();
	stats = new Stats();
	changedVerts = [];
	start = Date.now();

	stats.showPanel( 0 );
	document.body.appendChild( stats.dom );

	renderer.setSize( window.innerWidth, window.innerHeight );
	document.body.appendChild( renderer.domElement );
}

// Adding the Terrain
function addTerrain() {
	uniforms = {
		time: {
			type: 'f', // a float
			value: 0
		},
		res: {
			type: 'vec4',
			value: new THREE.Vector4(80, 40, 80, 50)
		},
		ripplePositions: {
			// Shader code has a max of 10 for this array so keep this in mind
			type: 'v3v',
			value: []
		}
	};

	for(let x = 0; x < 10; x++) {
		uniforms.ripplePositions.value[x] = new THREE.Vector3(0, 0, 0);
	}

	shaderMaterial = new THREE.ShaderMaterial({
		uniforms:     	uniforms,
		vertexShader:   document.getElementById("vertexshader").textContent,
		fragmentShader: document.getElementById("fragmentshader").textContent
	});
	shaderMaterial.extensions.derivatives = true;

	geometry = new THREE.PlaneBufferGeometry( uniforms.res.value.x, uniforms.res.value.y, uniforms.res.value.z, uniforms.res.value.w );

	vertCount = (uniforms.res.value.z + 1) * (uniforms.res.value.w + 1);
	let verts =  new Float32Array(vertCount);
	geometry.addAttribute('zState', new THREE.BufferAttribute(verts, 1.0));

	terrain = new THREE.Mesh(geometry, shaderMaterial);

	// add to scene
	scene.add(terrain);
	terrain.rotation.x = -90;
	terrain.position.z = 10;

	camera.position.y = 20;
	camera.rotation.x = -90;

	vertArr = terrain.geometry.attributes.zState.array;
}
// helper function
function adjacentVerts(vert) {
	//console.log(vert);
	var verts = [];

	verts.push(vert + 1); // right vert
	if ( ( vert ) % (uniforms.res.value.z + 1) != 0 ) {
		verts.push(vert - 1); // left vert
	}
	if ( ( vert + uniforms.res.value.z + 1) % (uniforms.res.value.z + 1) != 0 ) {
		verts.push(vert + uniforms.res.value.z); // top vert
	}
	verts.push(vert + uniforms.res.value.z + 1); // top vert
	verts.push(vert - uniforms.res.value.z - 1); // bottom vert

	return verts;
}

// Main Loop
function animate() {
	stats.begin();
	uniforms.time.value = .000025 * ( Date.now() - start );

	delta = clock.getDelta();

	for(var changedVert of changedVerts){
		vertArr[changedVert] > 0 ? vertArr[changedVert] -= delta / 10 : vertArr[changedVert] = 0;
		terrain.geometry.attributes.zState.needsUpdate = true;

		if (vertArr[changedVert] <= 0) {
			changedVerts = changedVerts.filter(function(e) { return e !== changedVert })
		}
	}

	renderer.render( scene, camera );
	stats.end();

	requestAnimationFrame( animate );
}

// blur the background when the mouse moves
function mouseBlur() {
	let raycaster = new THREE.Raycaster();
	let mouse = new THREE.Vector2();

	document.addEventListener("mousemove", function(event) {
		mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
		mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

		raycaster.setFromCamera( mouse, camera );

		var intersects = raycaster.intersectObjects([terrain]);

		if ( intersects.length > 0 ) {
			vertArr[intersects[ 0 ].face.a] = .2;
			changedVerts.push(intersects[ 0 ].face.a);

			let verts = adjacentVerts(intersects[ 0 ].face.a);
			for(var vert of verts){
				if (vert < vertArr.length && vert >= 0) {
					if(vertArr[vert] <= 0) {
						changedVerts.push(vert);
					}

					vertArr[vert] = ( vertArr[vert] > .1 ) ? .15 : .1;
				}

				let verts2 = adjacentVerts(vert);
				for(var vert2 of verts2){
					if (vert2 < vertArr.length && vert2 >= 0) {
						if(vertArr[vert2] <= 0) {
							changedVerts.push(vert2);
						}

						vertArr[vert2] = ( vertArr[vert2] > .05 ) ? .1 : .05;
					}
				}
			}
		}
	});
}

function clickRipple() {
	let raycaster = new THREE.Raycaster();
	let mouse = new THREE.Vector2();

	document.addEventListener("click", function(event) {
		mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
		mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

		raycaster.setFromCamera( mouse, camera );

		let intersects = raycaster.intersectObjects([terrain]);

		if ( intersects.length > 0 ) {
			let point = intersects[ 0 ].point;

			point.z = .000025 * ( Date.now() - start );

			uniforms.ripplePositions.value.shift();
			uniforms.ripplePositions.value.push(point);
			console.log(uniforms.ripplePositions.value);
		}
	});
}

setup();
addTerrain();
mouseBlur();
clickRipple();
animate();
